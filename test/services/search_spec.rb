require 'rails_helper'
require 'spec_helper'

describe Search do
  context '#find_by_query' do
    it 'compares result matches by query' do
      VCR.use_cassette('gsmarena_data') do
        query = 'Apple iPhone 7'
        response = Search.find_by_query(query)

        expect(response[0][:model_name] == 'iPhone 7').to be_truthy
      end
    end
  end

  context '#find_by_id' do
    it 'finds device by id' do
      VCR.use_cassette('gsmarena_data') do
        response = Search.find_by_id(8064)

        expect(response[:model_name] == 'iPhone 7').to be_truthy
      end
    end
  end
end
